NAME = push_swap

SOURCES = push_swap.c \
		operations.c \
		algorithm.c \
		algo.c \
		ft_atoi.c \
		ft_strlen.c \
		ft_putstr.c \
		ft_putchar.c \
		ft_putnbr.c \
		my_strjoin.c \
		my_print.c \
		small_algo.c

OBJECTS = $(subst .c,.o,$(SOURCES))

FLAGS = -Wall -Werror -Wextra

CC = gcc

RM = rm -rf

all: $(NAME)

$(NAME): $(OBJECTS)
	$(CC) $(FLAGS) -o $(NAME) $(OBJECTS)

clean:
	$(RM) $(OBJECTS)

fclean: clean
	$(RM) $(NAME)

re: fclean all clean

.PHONY: re fclean clean all
