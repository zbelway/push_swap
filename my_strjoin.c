/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   my_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 18:40:00 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/05 14:29:14 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		show_stack(t_push *ps, char op, char stack)
{
	int		i;

	if (op == 'R')
		ft_putstr("rr");
	else
		ft_putchar(op);
	ft_putchar(stack);
	ft_putstr("\nStack A: [");
	i = 0;
	while (i < ps->alen)
	{
		ft_putchar(' ');
		ft_putnbr(ps->a[i++]);
		ft_putchar(' ');
	}
	ft_putstr("]\nStack B: [");
	i = 0;
	while (i < ps->blen)
	{
		ft_putchar(' ');
		ft_putnbr(ps->b[i++]);
		ft_putchar(' ');
	}
	ft_putstr("]\n");
}

void		my_strjoin(t_push *ps, char op, char stack)
{
	int		i;
	int		j;
	char	*str;

	i = ft_strlen(ps->s) + 1;
	i += (op == 'R') ? 3 : 2;
	if (!(str = (char *)malloc(sizeof(char) * (i + 1))))
		return ;
	str[i] = '\0';
	j = -1;
	while (ps->s[++j])
		str[j] = ps->s[j];
	str[j++] = ' ';
	if (op == 'R')
	{
		str[j++] = 'r';
		str[j++] = 'r';
	}
	else
		str[j++] = op;
	str[j] = stack;
	ps->s = str;
	if (ps->show_stack)
		show_stack(ps, op, stack);
}
