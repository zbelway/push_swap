/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operations.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 23:38:20 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/03 17:53:46 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	swap(t_push *ps, char c)
{
	int	tmp;
	int	*tab;

	if (c == 'a')
		tab = ps->a;
	else
		tab = ps->b;
	tmp = tab[0];
	tab[0] = tab[1];
	tab[1] = tmp;
	my_strjoin(ps, 's', c);
}

void	push_front(t_push *ps, char c)
{
	int	i;
	int	*tab;
	int	ac;

	if (c == 'a')
	{
		ac = ps->blen;
		tab = ps->b;
		ps->blen--;
		ps->alen++;
	}
	else
	{
		ac = ps->alen;
		tab = ps->a;
		ps->alen--;
		ps->blen++;
	}
	i = 0;
	while (i < ac - 1)
	{
		tab[i] = tab[i + 1];
		i++;
	}
	tab[i] = 0;
}

void	push(t_push *ps, char c)
{
	int	i;
	int	tmp;
	int	*tab;
	int	ac;
	int	n;

	n = (c == 'a') ? ps->b[0] : ps->a[0];
	tab = (c == 'a') ? ps->a : ps->b;
	ac = (c == 'a') ? ps->alen + 1 : ps->blen + 1;
	i = 0;
	while (i < ac)
	{
		tmp = tab[i];
		tab[i++] = n;
		n = tmp;
	}
	push_front(ps, c);
	my_strjoin(ps, 'p', c);
}

void	rotate(t_push *ps, char c)
{
	int	i;
	int	last;
	int	*tab;
	int	ac;

	i = 0;
	if (c == 'a')
	{
		tab = ps->a;
		ac = ps->alen;
	}
	else
	{
		tab = ps->b;
		ac = ps->blen;
	}
	last = tab[0];
	while (i < ac - 1)
	{
		tab[i] = tab[i + 1];
		i++;
	}
	tab[i] = last;
	my_strjoin(ps, 'r', c);
}

void	reverse_rotate(t_push *ps, char c)
{
	int	i;
	int	first;
	int	*tab;

	if (c == 'a')
	{
		i = ps->alen - 1;
		tab = ps->a;
	}
	else
	{
		i = ps->blen - 1;
		tab = ps->b;
	}
	first = tab[i];
	while (i > 0)
	{
		tab[i] = tab[i - 1];
		i--;
	}
	tab[0] = first;
	my_strjoin(ps, 'R', c);
}
