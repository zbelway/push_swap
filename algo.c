/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 15:52:49 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/03 17:37:44 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

float		get_pivot(int *tab, int len)
{
	int		i;
	int		j;
	int		tmp;

	i = 0;
	while (i < len - 1)
	{
		j = i + 1;
		while (j < len)
		{
			if (tab[j] < tab[i])
			{
				tmp = tab[i];
				tab[i] = tab[j];
				tab[j] = tmp;
			}
			j++;
		}
		i++;
	}
	if (len % 2 == 1)
		return (tab[len / 2]);
	else
		return ((tab[len / 2] + tab[len / 2 - 1]) / 2.0);
}

int			*copy_tab(int *tab, int len)
{
	int		*cpy;
	int		i;

	i = 0;
	cpy = (int *)malloc(sizeof(int) * len);
	while (i < len)
	{
		cpy[i] = tab[i];
		i++;
	}
	return (cpy);
}
