/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithm.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/02 19:40:20 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/06 19:44:26 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			biggest_position(int *tab, int len)
{
	int		i;
	int		biggest;
	int		n;

	i = 1;
	n = 0;
	biggest = tab[0];
	while (i < len)
	{
		if (tab[i] > biggest)
		{
			biggest = tab[i];
			n = i;
		}
		i++;
	}
	return (n);
}

void		return_to_a(t_push *ps)
{
	int		n;

	while (ps->blen > 0)
	{
		n = biggest_position(ps->b, ps->blen);
		if (n != 0 && n < ps->blen / 2)
			while (n-- > 0)
				rotate(ps, 'b');
		else if (n != 0)
			while (n++ < ps->blen)
				reverse_rotate(ps, 'b');
		push(ps, 'a');
	}
}

void		reverse_run_algo(t_push *ps, int double_run)
{
	double	pivot;
	int		mid_point;

	while (ps->blen > 1)
	{
		mid_point = ps->blen / 2;
		pivot = get_pivot(copy_tab(ps->b, ps->blen), ps->blen);
		while (ps->blen - mid_point > 0)
		{
			if (ps->b[0] >= pivot)
				push(ps, 'a');
			else
				rotate(ps, 'b');
		}
	}
	run_algo(ps, double_run);
}

void		run_algo(t_push *ps, int double_run)
{
	double	pivot;
	int		mid_point;

	if (ps->alen < 13 || order(ps))
	{
		small_algo(ps);
		return ;
	}
	while (ps->alen > 1)
	{
		mid_point = ps->alen / 2;
		pivot = get_pivot(copy_tab(ps->a, ps->alen), ps->alen);
		while (ps->alen - mid_point > 0)
		{
			if (ps->a[0] <= pivot)
				push(ps, 'b');
			else
				rotate(ps, 'a');
		}
	}
	if ((double_run /= 40) > 0)
		reverse_run_algo(ps, double_run);
	return_to_a(ps);
}
