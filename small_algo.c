/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_algo.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/05 13:37:37 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/10 16:59:16 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			smallest_position(int *tab, int len)
{
	int		i;
	int		smallest;
	int		n;

	i = 1;
	n = 0;
	smallest = tab[0];
	while (i < len)
	{
		if (tab[i] < smallest)
		{
			smallest = tab[i];
			n = i;
		}
		i++;
	}
	return (n);
}

int			final_two(t_push *ps, int i, int j)
{
	while (i-- > 0)
		reverse_rotate(ps, 'a');
	swap(ps, 'a');
	while (j-- > 0)
		rotate(ps, 'a');
	return (1);
}

int			order(t_push *ps)
{
	int		i;

	i = 0;
	if (ps->alen == 4 && ps->a[0] > ps->a[1] && ps->a[0] > ps->a[2]
			&& ps->a[0] < ps->a[3] && ps->a[1] < ps->a[2])
		final_two(ps, 1, 2);
	while (i < ps->alen - 1)
	{
		if (ps->a[i] > ps->a[i + 1])
		{
			if (i == ps->alen - 2 && ps->alen > 3
					&& ps->a[i + 1] > ps->a[i - 1])
			{
				if (ps->alen != 4 || smallest_position(ps->a, ps->alen))
					return (final_two(ps, 2, 2));
			}
			return (0);
		}
		i++;
	}
	return (1);
}

void		smallest_algo(t_push *ps)
{
	if (ps->a[0] > ps->a[1] && ps->a[0] > ps->a[2])
	{
		rotate(ps, 'a');
		if (ps->a[0] > ps->a[1])
			swap(ps, 'a');
	}
	else if (ps->a[1] > ps->a[0] && ps->a[1] > ps->a[2])
	{
		reverse_rotate(ps, 'a');
		if (ps->a[0] > ps->a[1])
			swap(ps, 'a');
	}
	else if (ps->a[2] > ps->a[1] && ps->a[2] > ps->a[0])
		if (ps->a[0] > ps->a[1])
			swap(ps, 'a');
}

void		small_algo(t_push *ps)
{
	int		n;

	while (!order(ps))
	{
		n = smallest_position(ps->a, ps->alen);
		if (ps->alen == 3)
			smallest_algo(ps);
		else if (n != 0 && n <= ps->alen / 2)
			rotate(ps, 'a');
		else if (n != 0 && n > ps->alen / 2)
			reverse_rotate(ps, 'a');
		else if (n == 0)
			push(ps, 'b');
	}
	while (ps->blen > 0)
		push(ps, 'a');
}
