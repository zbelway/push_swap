/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 22:05:09 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/05 18:54:18 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			error(void)
{
	write(1, "Error\n", 6);
	exit(1);
	return (0);
}

int			parse_tab(t_push *ps)
{
	int		i;
	int		j;
	int		*tab;

	i = 0;
	tab = ps->a;
	while (i < ps->alen)
	{
		j = 0;
		while (j < ps->alen)
		{
			if (i != j && tab[i] == tab[j])
				return (error());
			j++;
		}
		i++;
	}
	return (1);
}

void		initialize(t_push *ps)
{
	int		i;

	i = 0;
	ps->s = (char *)malloc(sizeof(char));
	ps->s[0] = '\0';
	ps->blen = 0;
	while (i < ps->ac - 1)
		ps->b[i++] = 0;
	i = 0;
	while (i < ps->ac - 1)
		ps->a[i++] = 0;
	ps->color = 0;
	ps->command_count = 0;
	ps->show_stack = 0;
	ps->final_stack = 0;
	ps->blink = 0;
}

void		input_bonus(t_push *ps, char *s)
{
	while (*s)
	{
		if (*s == 'c')
			ps->color = 'c';
		else if (*s == 'C')
			ps->command_count = 'C';
		else if (*s == 's')
			ps->show_stack = 's';
		else if (*s == 'f')
			ps->final_stack = 'f';
		else if (*s == 'b')
			ps->blink = 'b';
		s++;
	}
}

int			main(int ac, char **av)
{
	int		i;
	int		j;
	t_push	ps;

	i = -1;
	j = 1;
	ps.ac = ac;
	ps.alen = ac - 1;
	ps.a = (int *)malloc(sizeof(int) * (ac - 1));
	ps.b = (int *)malloc(sizeof(int) * (ac - 1));
	initialize(&ps);
	if (ac > 1 && av[1][0] == '-' && (av[1][1] < '0' || av[1][1] > '9'))
	{
		input_bonus(&ps, av[1]++);
		j = 2;
		ps.alen--;
	}
	while (++i < ps.alen)
		ps.a[i] = ft_atoi(av[i + j]);
	if (ac > 2 && parse_tab(&ps))
		run_algo(&ps, ps.alen);
	ps_print(&ps, ++ps.s);
	return (0);
}
