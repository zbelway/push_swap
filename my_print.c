/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   my_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/21 17:27:26 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/05 15:24:22 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_putcolorstr(t_push *ps, const char *s)
{
	int	i;

	i = 0;
	while (s[i])
	{
		if (ps->color)
		{
			if (s[i] == 'r' && s[i + 1] == 'r')
				ft_putstr(RED);
			else if (s[i] == 'r' && s[i - 1] != 'r')
				ft_putstr(GREEN);
			else if (s[i] == 'p')
				ft_putstr(MAGENTA);
			else if (s[i] == 's')
				ft_putstr(CYAN);
		}
		ft_putchar(s[i]);
		i++;
	}
	if (ps->color)
		ft_putstr(NORMAL);
	ft_putchar('\n');
}

void	put_command_count(const char *s)
{
	int	i;

	i = 1;
	while (*s)
		if (*s++ == ' ')
			i++;
	ft_putstr("command count = ");
	ft_putnbr(i);
	ft_putchar('\n');
}

void	ps_print(t_push *ps, const char *s)
{
	int	i;

	if (ps->blink)
		ft_putstr(BLINK);
	ft_putcolorstr(ps, s);
	if (ps->final_stack)
	{
		i = 0;
		while (i < ps->alen)
		{
			ft_putstr("stack[");
			ft_putnbr(i);
			ft_putstr("] = ");
			ft_putnbr(ps->a[i]);
			ft_putchar('\n');
			i++;
		}
	}
	if (ps->command_count)
		put_command_count(s);
}
