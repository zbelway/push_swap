/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/15 14:45:19 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/05 18:38:19 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			ft_atoi(const char *str)
{
	long	n;
	int		sign;

	n = 0;
	sign = 1;
	while (*str == ' ' || *str == '\n' || *str == '\t' || *str == '\v'
			|| *str == '\f' || *str == '\r')
		str++;
	if (*str == '-' || *str == '+')
		sign = 44 - *str++;
	if (*str < '0' || *str > '9')
		return (error());
	while (*str == '0')
		str++;
	while (*str >= '0' && *str <= '9')
	{
		n *= 10;
		n += (*str++ - '0');
	}
	if (*str && (*str < '0' || *str > '9'))
		return (error());
	if (n > 2147483647 || n < -2147483648)
		return (error());
	return (n * sign);
}
