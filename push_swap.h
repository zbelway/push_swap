/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 22:04:25 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/05 18:54:35 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include <stdlib.h>
# include <unistd.h>

# define RED "\x1B[31m"
# define GREEN "\x1B[32m"
# define MAGENTA "\x1B[35m"
# define CYAN "\x1B[36m"
# define NORMAL "\x1B[0m"
# define BLINK "\e[5;37m"

typedef struct	s_push
{
	char		*s;
	int			n;
	int			ac;
	int			alen;
	int			blen;
	int			*a;
	int			*b;
	char		color;
	char		command_count;
	char		show_stack;
	char		final_stack;
	char		blink;
}				t_push;

int				ft_atoi(const char *str);
void			ft_putstr(const char *s);
void			ft_putnbr(int n);
void			ps_print(t_push *ps, const char *s);
size_t			ft_strlen(const char *str);
void			ft_putchar(char c);
void			push(t_push *ps, char c);
void			rotate(t_push *ps, char c);
void			reverse_rotate(t_push *ps, char c);
void			swap(t_push *ps, char c);
void			my_strjoin(t_push *ps, char op, char stack);
float			get_pivot(int *tab, int len);
int				error(void);
void			run_algo(t_push *ps, int double_run);
int				*copy_tab(int *tab, int len);
void			small_algo(t_push *ps);
int				order(t_push *ps);

#endif
